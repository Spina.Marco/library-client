import React, { Component } from 'react';

class AddBook extends Component {

    constructor(props) {
        super(props)

        this.state = {
            title: ''
        }
    }

    changeHandler = (e) => {
        this.setState({
            title: e.target.value
        })
    }

     postData = e => {
        e.preventDefault()
        var form = new FormData();
        form.set('title', this.state.title)

        try {
            fetch('http://localhost:8000/book', {
                method: 'post',
                body: form
            });
        } catch(e) {
            console.log(e)
        }
        window.location.reload()
        //return false;
    }

    render() {
        return (
           <div>
               <form onSubmit={this.postData}>
                   <input type="text" name="title" placeholder="Insert book title..." onChange={this.changeHandler} />
                   <button type="submit" className="submitButton">Submit </button>
               </form>
          </div>
        )
    }
}

export default AddBook;