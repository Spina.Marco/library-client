import React from 'react';

class Table extends React.Component {

    state = {
        loading : true,
        data: null
    }

    async componentDidMount() {
        const response = await fetch('/book')
        const data = await response.json()
        this.setState({data: data, loading: false})
    }

    deleteBook(id) {
        fetch('http://localhost:8000/book/' + id, {
            method: 'DELETE'
        })
        window.location.reload()
    }

    render() {
        return (
            <div>
                {this.state.loading ? <div>loading...</div> : 
                
                        <table>
                            <tbody>
                            <tr>
                                <th>id</th>
                                <th>Title</th>
                            </tr>
                        {this.state.data.map(book => (
                            <tr key ={book.id}>
                            <td>{book.id}</td>
                            <td>{book.title}</td>
                            <td><button onClick={() => this.deleteBook(book.id)}>Elimina</button></td>
                            <td><button>Modifica</button></td>
                            </tr>
                        ))}
                    </tbody>
                    </table>}
            </div>
        )
    }

}

export default Table;