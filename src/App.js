import React from 'react';
import Table from './Table'
import AddBook from './AddBook'

import './App.css';

function App() {
  return (
    <div className="App">
      <Table />
      <AddBook />
    </div>
  );
}

export default App;
